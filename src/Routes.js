import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';

import Login from './pages/Login';
import Signup from './pages/Signup';
import Beranda from './pages/Beranda';
import Daftarinfo from './pages/Daftarinfo';
import Daftarisiform from './pages/Daftarisiform';
import Daftarkirim from './pages/Daftarkirim';
import Daftartambah from './pages/Daftartambah';
import Profildojo from './pages/Profildojo';
import Ho from './pages/Ho';


export default class Routes extends Component {
	render() {
		return(
			<Router>
			    <Stack key="root" hideNavBar={true}>
			      <Scene key="login" component={Login} title="Login" initial={true}/>
			      <Scene key="signup" component={Signup} title="Register"/>
			      <Scene key="home" component={Beranda} title="Beranda"/>
				  <Scene key="profil" component={Profildojo} title="Register"/>
				  <Scene key="daftar" component={Daftarinfo} title="Register"/>
				  <Scene key="lanjutkan" component={Daftartambah} title="Register"/>
				  <Scene key="next" component={Daftarisiform} title="Register"/>
				  <Scene key="simpan" component={Daftarkirim} title="Register"/>
				  <Scene key="kirim" component={Ho} title="Register"/>
			    </Stack>
			 </Router>
			)
	}
}