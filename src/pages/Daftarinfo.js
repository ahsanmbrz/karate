/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet } from 'react-native';
import { Image , TouchableOpacity,ScrollView } from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import Iconsa from 'react-native-vector-icons/AntDesign';
import Iconsi from 'react-native-vector-icons/MaterialIcons';
import {Actions} from 'react-native-router-flux';
import { Badge, Card, CardItem, Container, Header, Content, Footer, FooterTab, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Icon, Title, View, Radio,  } from "native-base";
export default class CardItemBordered extends Component {
  goBack() {
    Actions.pop();
}
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
      
        <Header style={{backgroundColor:'#5a113d'}}>
          <Left>
          <TouchableOpacity onPress={this.goBack}>
              <Icon name='arrow-back' style={{fontSize: 30, color: 'white'}} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title> Pendaftaran </Title>
          </Body>
        </Header>

        
       
        <View style={{ justifyContent: 'center', flexDirection: 'row' , top:10}} >
                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/wann.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }} > Info </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/numtu.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Tambah </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/tiga.png')} style={{ width: 25, height: 25 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Isi Form </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/numfo.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Kirim </Text>
                    </Button>

                </View>
<ScrollView>
            <View style={{ top: 30, width: 332, height: 224, left: 20}}>
              <Text style={{ color : 'red'}}> PERSYARATAN ! </Text>
            </View>
        
            <View style={{ top: -170 , left: 29 , fontSize : 12 ,  fontFamily: "Roboto" }}>
              <Text> 
              1. Berstatus Siswa/Siswi SD, SMP, SMA 
              </Text>
              <Text>
              2. Tidak Mempunyai Riwayat Penyakit Keras
              </Text>
              <Text>
              3. Rajin
              </Text>
              <Text>
              4. Giat
              </Text>
              <Text>
              5. Disiplin
              </Text>
              </View>

              <View style={{ top: -155 , left: 29 , fontSize : 12 ,  fontFamily: "Roboto" }}>
              <Text>
              Khusus Pendaftaran Tahun Ajaran 2019/2020 Untuk :
              </Text>
              <Text> 
              BB Laki-laki        :  +70 kg
              </Text>
              <Text>
              BB Perempuan    :  +57 kg
              </Text>
              <Text>
              TB Laki-laki        :  165 cm
              </Text>
              <Text>
              TB Perempuan    :  157 cm 
              </Text>
              <Text> 
              Akan Mendapatkan DISKON 50 % Biaya Administrasi
              </Text>
              </View>

              <View style={{ top: -145 , left: 29 , fontSize : 12 ,  fontFamily: "Roboto" }}>
              <Text>
              Dan Bagi Peserta/Karateka yang lolos menjadi Atlit (Juara),
              </Text>
              <Text>
              Akan di beri "BEASISWA BEBAS SPP KARATE"
              </Text>
                 </View>
            

            <Content>
                    <List style={{ }}>
                        <ListItem>
                            <Radio selected={false} />
                            <Text>Saya Tidak Setuju</Text>
                        </ListItem>
                        <ListItem>
                            <Radio selected={true} />
                            <Text>Saya Setuju</Text>
                        </ListItem>
                    </List>
                </Content>
            
        
        
         <Footer style={{backgroundColor:'#4a0931'}}>
          <FooterTab style={{backgroundColor:'#4a0931'}}>
            <Button vertical onPress={() => navigate('lanjutkan')} >
              <Text style={{fontSize: 16, color: '#ffffff'}}>LANJUTKAN</Text>
            </Button>
            
          </FooterTab>
        </Footer>
        </ScrollView>
      </Container>
    );
  }
}