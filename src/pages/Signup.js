import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Button,
  ToastAndroid,
  ScrollView,
  Picker
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {Actions} from 'react-native-router-flux';

export default class Signup extends Component<{}> {
constructor(props) {
    super(props);
  
    this.state = {
      username: '',
      password: '',
      ulang:'',
      name:'',
      tempatlahir:'',
      tanggallahir:new Date(),
      rt:'',
      rw:'',
      kelurahan:'',
      kecamatan:'',
      kota:'',
      prov:'',
      kyu:'',
      dojo:'',
      lokasi:''
    };
  }
  onRegis() {
   const { username, password, ulang, name, tempatlahir, tanggallahir, rt, rw, kelurahan,kecamatan,kota,prov,kyu,dojo,lokasi } = this.state;
   if( password!=ulang ){
     ToastAndroid.show('Password tidak sama', ToastAndroid.SHORT);
   }else if(password.length < 8) {

ToastAndroid.show('Password kurang panjang', ToastAndroid.SHORT);
   }
   else{
    console.log(username);
    console.log(password);
     fetch("http://192.168.0.30:3000/users/register", {
        method: 'POST',
        body: JSON.stringify({
          username: username,
          password:password,
          name:name,
          tempatlahir:tempatlahir,
          tanggallahir:tanggallahir,
          rt:rt,
          rw:rw,
          kelurahan:kelurahan,
          kecamatan:kecamatan,
          kota:kota,
          prov:prov,
          kyu:kyu,
          dojo:dojo,
          lokasi:lokasi
        }),
        headers: {
          "Content-type": "application/json"
        }
      }).then(res => res.json())
        .then(
          (result) => {
            console.log(result)
            if(result.status==="success"){
              ToastAndroid.show('signup', ToastAndroid.SHORT);
            this.signup();
            }else{
            ToastAndroid.show('error', ToastAndroid.SHORT);
          }
          },
          (error) => {
          }
        );
        Actions.login();
   }

    
    }
  signup() {
    Actions.signup()
  }
  render(){
    return(
      <ScrollView>
      <View style={styles.container}>
      <Text>Isi Kelengkapan Registrasi Di Bawah Ini</Text>
       <TextInput
          value={this.state.username}
          onChangeText={(username) => this.setState({ username })}
          placeholder={'Username'}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder={'Password'}
          secureTextEntry={true}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.ulang}
          onChangeText={(ulang) => this.setState({ ulang })}
          placeholder={'Ulangi Password'}
          secureTextEntry={true}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.name}
          onChangeText={(name) => this.setState({ name })}
          placeholder={'nama lengkap'}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.tempatlahir}
          onChangeText={(tempatlahir) => this.setState({ tempatlahir })}
          placeholder={'Tempat Lahit'}
          style={styles.inputBox}
        />
         <Text style={{marginBottom:3}}>Tanggal Lahir</Text>
        <DatePicker
          style={{width: 200}}
          date={this.state.tanggallahir}
          mode="date"
          placeholder="Tanggal Lahir"
          format="YYYY-MM-DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
        }}
        onDateChange={(tanggallahir) => {this.setState({tanggallahir})}}
        />
        <TextInput
          value={this.state.prov}
          onChangeText={(prov) => this.setState({ prov })}
          placeholder={'Provinsi'}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.kota}
          onChangeText={(kota) => this.setState({ kota })}
          placeholder={'Kota'}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.kecamatan}
          onChangeText={(kecamatan) => this.setState({ kecamatan })}
          placeholder={'Kecamatan'}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.kelurahan}
          onChangeText={(kelurahan) => this.setState({ kelurahan })}
          placeholder={'kelurahan'}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.rw}
          onChangeText={(rw) => this.setState({ rw })}
          placeholder={'RW'}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.rt}
          onChangeText={(rt) => this.setState({ rt })}
          placeholder={'RT'}
          style={styles.inputBox}
        />
        <Picker
          selectedValue={this.state.kyu}
          onValueChange={(kyu) => this.setState({ kyu })}
          style={{height: 50, width: '80%'}}>
          <Picker.Item label="Pilih Kyu" value="" />
          <Picker.Item label="Kyu 10" value="10" />
          <Picker.Item label="Kyu 9" value="9" />
          <Picker.Item label="Kyu 8" value="8" />
          <Picker.Item label="Kyu 7" value="7" />
          <Picker.Item label="Kyu 6" value="6" />
          <Picker.Item label="Kyu 5" value="5" />
          <Picker.Item label="Kyu 4" value="4" />
          <Picker.Item label="Kyu 3" value="3" />
          <Picker.Item label="Kyu 2" value="2" />
          <Picker.Item label="Kyu 1" value="1" />
        </Picker>
        <TextInput
          value={this.state.dojo}
          onChangeText={(dojo) => this.setState({ dojo })}
          placeholder={'Dojo'}
          style={styles.inputBox}
        />
        <TextInput
          value={this.state.lokasi}
          onChangeText={(lokasi) => this.setState({ lokasi })}
          placeholder={'Lokasi Dojo'}
          style={styles.inputBox}
        />

        <TouchableOpacity style={styles.button}onPress={this.onRegis.bind(this)}>
               <Text style={styles.buttonText}>REGISTRASI</Text>
        </TouchableOpacity>
      </View>
      </ScrollView>
      )
  }
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center',
    paddingHorizontal:10,
    paddingVertical:10
  },
  bgimage :{
    flex :1 ,
    alignSelf:'stretch',
    width: null,
    justifyContent:'center'

  },
  content:{
    alignItems:'center'

  },
  inputBox: {
    width:334,
    height:40,
    backgroundColor:'#ffffff',
    borderRadius: 6,
    borderWidth:1,
    paddingHorizontal:16,
    fontSize:14,
    color:'#373837',
    marginVertical: 10
  },
  button: {
    width:334,
    height:40,
    backgroundColor:'#310522',
     borderRadius: 6,
     borderWidth:1,
      marginVertical: 10,
      paddingVertical: 10
  },
  buttonText: {
    fontSize:16,
    fontFamily: 'sans-serif',
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }

});
