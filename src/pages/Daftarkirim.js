/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet } from 'react-native';
import { Image } from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import Iconsa from 'react-native-vector-icons/AntDesign';
import Iconsi from 'react-native-vector-icons/MaterialIcons';
import Iconsu from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconse from 'react-native-vector-icons/Foundation';
import { Badge, Card, CardItem, Container, Header, Content, Footer, FooterTab, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Icon, Title, View, Radio,  } from "native-base";
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
export default class CardItemBordered extends Component {

  state = {
    name: '',
};

state = {
    checked: false,
};


  render() {

    const { navigate } = this.props.navigation;

        let data = [{
            value: 'Orang Tua',
        }, {
            value: 'Anak',
        }, {
            value: 'Sepupu',
        }];

    
    return (
      <Container>
        <Header style={{backgroundColor:'#5a113d'}}>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title> Pendaftaran </Title>
          </Body>
        </Header>

        <View style={{ justifyContent: 'center', flexDirection: 'row' , top:10}} >
                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/wann.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }} > Info </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/numtu.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Tambah </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/numtri.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Isi Form </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/fo.png')} style={{ width: 25, height: 25 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Kirim </Text>
                    </Button>

                </View>
       

                <View style={{ top: 18 }} >
                    <Text>      Pilih Pendaftar                                    Tambah Jumlah Kohai </Text>
                </View>


                <Content>
                    <View style={{ left: 10, width: 135 }}>
                        <Dropdown
                            label='Pilih Pendaftaran'
                            data={data}

                        />
                    </View>

                   
                 <Button transparent style={{ left : 130, height:40}}>
                    
                    <Iconsa name='minuscircleo' style={{fontSize: 20, color: '#a7a7a7'}}/>
                   
                    <Text> 5 </Text>
                
                    <Iconsa name='pluscircleo' style={{fontSize: 20, color: '#008466'}}/>
                   
                </Button>
          

                    <Card >
                    <CardItem  style = {{ width: 332, height: 80, borderRadius: 60, borderStyle: "solid" }}>   
                            <Left>          
                            <Iconsu name ="account-box" style={{ fontSize: 30, color: '#757575' }} />
                            </Left>
                            <Text style= {{ fontSize: 20 }}>Rizal Ahmad Hasan</Text>
                            <Right>
                            <Text style={{ textDecorationLine: 'underline', left : 80, fontSize: 20, color: '#008466' }} > Edit </Text>
                            </Right>
                        </CardItem>
                   </Card>

                   <Card >
                   <CardItem style = {{ width: 332, height: 80, borderRadius: 60 }} > 
                            <Left>
                            <Iconsu name ="account-box" style={{ fontSize: 30, color: '#757575' }} />
                            </Left>
                            <Text style= {{ fontSize: 20 }}>Ramdanni</Text>
                            <Right>
                            <Text style={{ textDecorationLine: 'underline', left : 80, fontSize: 20, color: '#008466' }} > Edit </Text>
                            </Right>
                        </CardItem>
                   </Card>

                   <Card>
                        <CardItem style = {{ width: 332, height: 80, borderRadius: 60 }}>              
                            <Left>
                            <Iconsu name ="account-box" style={{ fontSize: 30, color: '#757575' }} />
                            </Left>
                            <Text style= {{ fontSize: 20 }}>Marfuah</Text>
                            <Right>
                            <Text style={{ textDecorationLine: 'underline', left : 80, fontSize: 20, color: '#008466' }} > Edit </Text>
                            </Right>
                        </CardItem>
                   </Card>

                   <Card>
                   <CardItem style = {{ width: 332, height: 80, borderRadius: 60 }}> 
                             <Left>  
                            <Iconsu name ="account-box" style={{ fontSize: 30, color: '#757575' }} />
                            </Left>
                            <Text style= {{ fontSize: 20 }}>Malih</Text>
                            <Right>
                            <Text style={{ textDecorationLine: 'underline', left : 80, fontSize: 20, color: '#008466' }} > Edit </Text>
                            </Right>
                        </CardItem>
                   </Card>

                   <Card>
                   <CardItem style = {{ width: 332, height: 80, borderRadius: 60 }}>              
                            <Left>
                            <Iconsu name ="account-box" style={{ fontSize: 30, color: '#757575' }} />
                            </Left>
                            <Text style= {{ fontSize: 20 }}>Bambang</Text>
                            <Right>
                            <Text style={{ textDecorationLine: 'underline', left : 80, fontSize: 20, color: '#008466' }} > Edit </Text>
                            </Right>
                        </CardItem>
                   </Card>
                </Content>

                <Footer style={{backgroundColor:'#4a0931'}}>
          <FooterTab style={{backgroundColor:'#4a0931'}}>
            <Button vertical onPress={() => navigate('kirim')} >
              <Text style={{fontSize: 16, color: '#ffffff'}}>KIRIM</Text>
            </Button>
            
          </FooterTab>
        </Footer>

                   
                
      </Container>
    );
  }
}