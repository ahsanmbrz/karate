/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, ScrollView } from 'react-native';
import { Image , TouchableOpacity, ActivityIndicator } from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import Iconsa from 'react-native-vector-icons/AntDesign';
import {Actions} from 'react-native-router-flux';
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import MapView, { Marker } from 'react-native-maps';
import { Card, CardItem, Container, Badge, Header, Content, Footer, FooterTab, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Icon, Title, View, Fab, Accordion, Separator } from "native-base";

export default class CardItemBordered extends Component {
  constructor(props) {
    super(props);
  
    this.state = {
      gambarutama:require('../image/pic1.jpg')
    };
  }
  goBack() {
    Actions.pop();
}
gantigambar =gambar =>() => {
    this.setState({
      gambarutama: gambar
    })
  }

  render() {
    const { navigate } = this.props.navigation;
    const hasil = this.props.navigation.getParam('result','none');
    const region = {
      latitude:hasil.latlong.latitude,
      longitude:hasil.latlong.longitude,
      latitudeDelta:0.0922,
      longitudeDelta:0.0421
    };
    console.log(region);
    return (
      <Container>
       <Header transparent>
          <Left>
            <TouchableOpacity onPress={this.goBack}>
              <Icon name='arrow-back' style={{fontSize: 30, color: 'black'}} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title>Transparent</Title>
          </Body>
          <Right>
            <Button style={{ backgroundColor: 'white', borderRadius: 30 }}>
              <Icon name="md-share"style={{color: 'black' }}/>
            </Button>
          </Right>
        </Header>
        <Content>
          {/* video player */}
          <View style={{flex: 1, flexDirection: 'row', alignItems:'stretch'}}>
            <Image source={this.state.gambarutama} style={{ height: 250, width:'100%' }} />
            </View>
            <View style={{flex: 1, flexDirection: 'row', alignItems:'stretch'}}>
             <ScrollView horizontal={true} >
            <TouchableOpacity onPress={this.gantigambar(require('../image/pic1.jpg'))}>
                <Image vertical source={require('../image/pic1.jpg')} style={{  width: 105, height: 85 }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.gantigambar(require('../image/pic2.jpg'))}>
                <Image vertical source={require('../image/pic2.jpg')} style={{  width: 105, height: 85 }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.gantigambar(require('../image/pic3.jpg'))}>
                <Image vertical source={require('../image/pic3.jpg')} style={{  width: 105, height: 85 }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.gantigambar(require('../image/pic4.jpg'))}>
                <Image vertical source={require('../image/pic4.jpg')} style={{  width: 105, height: 85 }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.gantigambar(require('../image/pic5.jpg'))}>
                <Image vertical source={require('../image/pic5.jpg')} style={{  width: 105, height: 85 }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.gantigambar(require('../image/pic1.jpg'))}>
                <Image vertical source={require('../image/pic1.jpg')} style={{  width: 105, height: 85 }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.gantigambar(require('../image/pic2.jpg'))}>
                <Image vertical source={require('../image/pic2.jpg')} style={{  width: 105, height: 85 }} />
            </TouchableOpacity>
              </ScrollView>
             
              </View>
              

            <Card transparent>
            <CardItem>
              <Left>
              <Text style={{fontSize: 29, color: '#000'}}>{hasil.name}</Text>
                 <Button transparent style={{justifyContent:'center', height:40 , left : -168 , top : 25 }}>
             <Icons name='star' style={{ fontSize : 10 , color: '#f7ca18'}}/>
             <Icons name='star' style={{ fontSize : 10 , color: '#f7ca18'}}/>
             <Icons name='star' style={{ fontSize : 10 , color: '#f7ca18'}}/>
             <Icons name='star' style={{ fontSize : 10 , color: '#f7ca18'}}/>
             </Button>
             </Left>
             </CardItem>
            </Card>
            <Card transparent>
            <CardItem>
             <Button transparent style={{height:10 , top : -15}}>
             <Icons name="map-marker" style={{ left : 15 , fontSize: 20, color: '#95989a'}}/>
             <Text style={{ left : 5 , fontSize: 14, color: 'black'}}>{hasil.alamat}</Text>
            </Button>
            </CardItem>
            </Card>
            
           
            <View>
    <Collapse>
      <CollapseHeader>
        <Separator bordered style = {{ height: 70 }}>
        <Text style={{fontSize: 20 , color: '#000000'}}>Fasilitas</Text>
        </Separator>
      </CollapseHeader>

      <CollapseBody>
      { 
          hasil.fasilitas.map((item, key)=>(
        <ListItem >
          <Text>{item}</Text>
        </ListItem>
        )
        )}
        
      </CollapseBody>
    </Collapse>


<Collapse>
  <CollapseHeader>
    <Separator bordered style = {{ height: 70 }}>
    <Text style={{fontSize: 20 , color: '#000000'}}>Visi</Text>
    </Separator>
  </CollapseHeader>

  <CollapseBody>
    <ListItem style = {{ height: 100 }} >
      
      <Text style = {{ padding:2 }}>{hasil.visi}
      </Text> 
     {/* <Text style = {{ left : -380 ,top : 15 }}> 2. Mengutamakan Kualitas di atas Kuantitas </Text> */}
      
      
    </ListItem>

  </CollapseBody>
</Collapse>


<Collapse>
  <CollapseHeader>
    <Separator bordered style = {{ height: 70 }}>
    <Text style={{fontSize: 20 , color: '#000000'}}>Misi</Text>
    </Separator>
  </CollapseHeader>

  <CollapseBody>
    <ListItem style = {{ height: 190 }} >
      
      <Text style = {{ padding:2 }}>{hasil.misi}
      </Text> 
      
    </ListItem>

  </CollapseBody>
</Collapse>

<Collapse>
  <CollapseHeader>
    <Separator bordered style = {{ height: 70 }}>
    <Text style={{fontSize: 20 , color: '#000000'}}>Struktur Organisasi</Text>
    </Separator>
  </CollapseHeader>

  <CollapseBody>
    <ListItem style = {{ height: 190 }} >
      
      <View> 

      <Image source={ require('../image/struktur.png')} style={{ justifyContent:'center', width: 300, height: 148, left : 44 }} />
     
      </View> 
      
    </ListItem>

  </CollapseBody>
</Collapse>

<Collapse>
  <CollapseHeader>
    <Separator bordered style = {{ height: 70 }}>
    <Text style={{fontSize: 20 , color: '#000000'}}>Alamat</Text>
    </Separator>
  </CollapseHeader>

  <CollapseBody>
    <ListItem style = {{ height: 250 }} >
      
      <View> 
       
                <MapView 
                 toolbarEnabled={false}
                 initialRegion={region}
                 style={{ width: 330, height: 168 , left : 30 , top : -20}}>
                 <MapView.Marker
                    coordinate={hasil.latlong}
                    title={hasil.name}
                    description={hasil.alamat}
                    pinColor='red'
                 /> 
                 </MapView>
     
      </View> 

      <Button transparent light style = {{ left : -330 , top : 95 }}>
              <Icons name="building" style={{fontSize: 20, color: '#95989a'}}/>
              <Text style={{fontSize: 15, color: 'black'}}>{hasil.alamat}</Text>
          </Button>
      
    </ListItem>

  </CollapseBody>
</Collapse>


<Collapse>
  <CollapseHeader>
    <Separator bordered style = {{ height: 70 }}>
    <Text style={{fontSize: 20 , color: '#000000'}}>Lain-lain</Text>
    </Separator>
  </CollapseHeader>

  <CollapseBody>
    <ListItem style = {{ height: 100 }} >
      
      <Text style = {{ top : -20 }}>{hasil.lain}</Text> 
      
      
    </ListItem>

  </CollapseBody>
</Collapse>

</View>
</Content>

          <View style={{position:'absolute', bottom:'10%' , alignSelf: 'center' , justifyContent: 'center'}}>
        <Button  style={{backgroundColor: 'white',flex: 1 , width:70, height:70 , borderRadius: 50, borderWidth:1,borderColor:'#2a001a', alignItems:'center', justifyContent:'center'}} >
              <Iconsa name='upcircle' style={{ fontSize: 56,color: '#4a0931' , alignItems:'center', justifyContent:'center'}}/>
              </Button>
          </View>

        
         <Footer style={{backgroundColor:'#4a0931'}}>
          <FooterTab style={{backgroundColor:'#4a0931'}}>
            <Button vertical onPress={() => navigate('daftar')} >
              <Text style={{fontSize: 12, color: '#ffffff'}}>DAFTAR</Text>
            </Button>
            
          </FooterTab>
        </Footer>
      
      </Container>
    );
  }
}