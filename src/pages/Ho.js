/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet } from 'react-native';
import { Image } from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import Iconsa from 'react-native-vector-icons/AntDesign';
import Iconsi from 'react-native-vector-icons/MaterialIcons';
import { Badge, Card, CardItem, Container, Header, Content, Footer, FooterTab, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Icon, Title, View, Radio,  } from "native-base";
export default class CardItemBordered extends Component {
  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#5a113d'}}>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Pemberitahuan</Title>
          </Body>
        </Header>

        
       
            <View style={{ top : 15, left: 150, backgroundColor:'white' }}>
            
            <Iconsa name="checkcircle" style={{fontSize: 100, color: '#008466'}}/>
            <Text style={{fontSize: 17,color: '#008466'}} > Berhasil Terkirim </Text>
        
            </View>

            <View>
            <Button full light style={{ top: 59}}>
            <Text style = {{  color: 'red'}}> Perhatian ! </Text>
          </Button>
          <Button full light style={{ top: 59}}>
            <Text>Silahkan Konfirmasi QR Code Berikut kepada Manager/Pelatih Dojo Untuk di SCAN !</Text>
          </Button>
            </View>
        
            <View style={{ top : 90, left: 50,}}>
            
            <Image source = {require('../image/kode.jpg')}style={{fontSize: 80}}/>
        
            </View>
            
    
      </Container>
    );
  }
}