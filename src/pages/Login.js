import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar ,
  TouchableOpacity,
  Image,
  ImageBackground
} from 'react-native';

import Logo from '../components/Logo';
import Form from '../components/Form';

import {Actions} from 'react-native-router-flux';



export default class Login extends Component<{}> {
  constructor(props) {
    super(props);
  
    this.state = {};
  }
	render() {
		return(
      
			<View style={styles.container}>
      <ImageBackground source={require('../image/gam.png')} style={{width: '100%', height: '100%',alignItems:'center'}}>
      
      <Text>K-DOJO</Text>
				<Logo/>
				<Form type="Login"/> 
				{/*<View style={styles.signupTextCont}>
					<TouchableOpacity onPress={this.signup}><Text style={styles.signupButton}> USER ADMIN</Text></TouchableOpacity>
				</View>*/}
         

        </ImageBackground>

			</View>
       
			)
	}
}

const styles = StyleSheet.create({
  container : {
    backgroundColor:'#ffffff',
    flex: 1,
    alignItems:'center',
    justifyContent :'center'
  },
  signupTextCont : {
  	flexGrow: 1,
    alignItems:'flex-end',
    justifyContent :'center',
    paddingVertical:16,
    flexDirection:'row'
  },
  signupText: {
  	color:'rgba(255,255,255,0.6)',
  	fontSize:25,
    color:'black'
  },
  signupButton: {
  	color:'#310522',
  	fontSize:16,
  	fontWeight:'500'
  }
});
