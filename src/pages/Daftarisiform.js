/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
    Text,
    View,
    Image,
    PanResponder,
    TextInput,
    Picker,
    ActivityIndicator,
    PermissionsAndroid,
    Alert,
    Platform
} from 'react-native';

import Iconsi from 'react-native-vector-icons/MaterialIcons';
import { Container, Header, Content, Footer, FooterTab, Left, Body, Button, Icon, Title} from "native-base";
import DatePicker from 'react-native-datepicker';
import { RadioButton, Checkbox } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import {Actions} from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';


export default class datepicker extends Component {
    state = {
        name: '',
    };

    state = {
        checked: false,
    };


    constructor(props) {
        super(props);

        this.state = {
            date: '',
            value:null,
            time: '20:00',
            datetime: '2016-05-05 20:00',
            datetime1: '2016-05-05 20:00',
            provinsi: [ { id: '11', nama: 'Aceh' },
            { id: '12', nama: 'Sumatera Utara' }],
            provstring:'11',
            kab:null,
            iskab:false,
            kabstring:'123',
            kec:null,
            kecstring:'21',
            iskec:false,
            desastring:'null',
            desa:null,
            isdesa:false,
            index:null,
            pendaftar:null,
            currentdata:null,
            islemkari:false,
            pengprovstring:'null',
            ispengcab:false,
            pengcab:null,
            pengcabstring:'null',
            dojo:null,
            isbeladirilain:false,
            perguruan:null,
            pengalamanlain:null,
            islemkarival:'',
            photo:null,
            photourl:null
        };
    }
  handleChoosePhoto = () => {
    const options = {
      title: "Select a photo",
      takePhotoButtonTitle: "Take a photo",
      chooseFromLibraryButtonTitle: "Choose from gallery",
      quality: 1
    }
    ImagePicker.showImagePicker(options, (response) => {
        if (response.uri) {
        this.setState({ photo: response })
        console.log(response)
        this.handleUploadPhoto();
      }
    });
    // 
  }
    handleUploadPhoto = () => {
      var data = new FormData();
        data.append('wallpaper', {
          uri:  this.state.photo.uri,
          type: this.state.photo.type, // or photo.type
          name: this.state.photo.fileName
        });
        fetch('http://192.168.0.22:3000/users/upload', {
          method: 'POST',
           headers: {
              "Content-Type": "multipart/form-data",
            },
          body: data
        }).then(res => res.json()).then(
        (result) => {
            console.log(result)
            this.setState({
                photourl:result.address
            })
        },(error) => {console.log(error);});
    };
     async permissioncheck(){
       await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          PermissionsAndroid.WRITE_EXTERNAL_STORAGE,
          PermissionsAndroid.READ_EXTERNAL_STORAGE,
          {
            title: "Location Accessing Permission",
            message: "App needs access to your location"
          }
        );
      }
    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (e) => { console.log('onStartShouldSetPanResponder'); return true; },
            onMoveShouldSetPanResponder: (e) => { console.log('onMoveShouldSetPanResponder'); return true; },
            onPanResponderGrant: (e) => console.log('onPanResponderGrant'),
            onPanResponderMove: (e) => console.log('onPanResponderMove'),
            onPanResponderRelease: (e) => console.log('onPanResponderRelease'),
            onPanResponderTerminate: (e) => console.log('onPanResponderTerminate')
        });
    }
    async componentDidMount(){
        this.permissioncheck();
      fetch("http://dev.farizdotid.com/api/daerahindonesia/provinsi", {
      method: 'GET',
      headers: {
        "Content-type": "application/json"
      }
    }).then(res => res.json())
      .then(
        (result) => {
            this.setState({
              provinsi:result.semuaprovinsi,
              provstring:result.semuaprovinsi[0].nama
            })},(error) => {console.log(error);});
        var index=this.props.index.key;
        var pendaftar = this.props.pendaftar;
        console.log(pendaftar);
        this.setState({
            index:index,
            pendaftar:pendaftar
        })  ;
        if(this.props.pendaftar==='diri sendiri'){
           await AsyncStorage.removeItem('@registereduser'); 
        }else{
            var hehe = await AsyncStorage.getItem('@registereduser');
            if (hehe!=null){
                this.setState({
                    currentdata:hehe
                });
            }
        }
        
    }

    getkab(provstring){
        this.setState({
            provstring:provstring
        });
        const result = this.state.provinsi.find( prov => prov.nama === provstring );
        let kabupaten=null;
        if(result!=null){
             fetch("http://dev.farizdotid.com/api/daerahindonesia/provinsi/"+result.id+"/kabupaten", {
              method: 'GET',
              headers: {
            "Content-type": "application/json"}}).then(res => res.json())
          .then(
            (hasil) => {
                if(hasil.kabupatens!=null){
                    console.log(hasil.kabupatens)
                    this.setState({
                        kab:hasil.kabupatens,
                        kabstring:hasil.kabupatens[0].nama
                    });
                    this.setkabstat();
                }

            },(error) => {console.log(error);});
        }
    }
    setkabstat(){
        this.setState({
            iskab:true
        })
    }
    getkec(kabstring){
        this.setState({
            kabstring:kabstring
        });
        const hasil = this.state.kab.find( kab => kab.nama === kabstring );
        console.log(hasil.id);
        if(hasil!=null){
             fetch("http://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/"+hasil.id+"/kecamatan", {
              method: 'GET',
              headers: {
            "Content-type": "application/json"}}).then(res => res.json())
          .then(
            (hasil) => {
                if(hasil.kecamatans!=null){
                   
                    this.setState({
                        kec:hasil.kecamatans,
                        kecstring:hasil.kecamatans[0].nama
                    });
                    this.setkecstat();
                }

            },(error) => {console.log(error);});
        }
    }
    setkecstat(){
        console.log('ini diekse')
        this.setState({
            iskec:true
        })
    }
    getdes(kecstring){
        this.setState({
            kecstring:kecstring
        });

        const hasil = this.state.kec.find( kec => kec.nama === kecstring );
        console.log(hasil.id);
        if(hasil!=null){
             fetch("http://dev.farizdotid.com/api/daerahindonesia/provinsi/kabupaten/kecamatan/"+hasil.id+"/desa", {
              method: 'GET',
              headers: {
            "Content-type": "application/json"}}).then(res => res.json())
          .then(
            (hasil) => {
                if(hasil.desas!=null){
                    console.log(hasil.desas)
                    this.setState({
                        desa:hasil.desas,
                        desastring:hasil.desas[0].nama
                    });
                    this.setdesstat();
                }

            },(error) => {console.log(error);});
        }
    }
    setdesstat(){
        this.setState({
            isdesa:true
        });
    }
    createpicker(){
        let itemsource = this.state.provinsi.map((item, key) =>  <Picker.Item label={item.nama} value={item.nama} />);
        return(
        <Picker
          selectedValue={this.state.provstring}
          onValueChange={provstring=>this.getkab(provstring)}   
          style={{ left:30,alignItems:'center', top: -100}}
          >
          {itemsource}
        </Picker>
        );
    }
    getpengcab(pengprovstring){
        this.setState({
            pengprovstring:pengprovstring
        });
        console.log(pengprovstring)
        const result = this.state.provinsi.find( prov => prov.nama === pengprovstring );
        let kabupaten=null;
        if(result!=null){
             fetch("http://dev.farizdotid.com/api/daerahindonesia/provinsi/"+result.id+"/kabupaten", {
              method: 'GET',
              headers: {
            "Content-type": "application/json"}}).then(res => res.json())
          .then(
            (hasil) => {
                if(hasil.kabupatens!=null){
                    console.log(hasil.kabupatens)
                    this.setState({
                        pengcab:hasil.kabupatens,
                        pengcabstring:hasil.kabupatens[0].nama
                    });
                    this.setpengcabstat();
                }

            },(error) => {console.log(error);});
        }
    }
    setpengcabstat(){
        this.setState({
            ispengcab:true
        });
    }
    async onRegis(user) {

        const { nama, value, tempatlahir, date, provstring, kabstring, kecstring, desastring, alamat } = this.state;
        if(this.state.currentdata!=null){
            var tampungan = JSON.parse(this.state.currentdata); 
        }else{
            var tampungan=[];
        }

        tampungan[this.state.index]= {
            nama:nama,
            jeniskelamin:value,
            tempatlahir:tempatlahir,
            tanggallahir:date,
            provinsi:provstring,
            kota:kabstring,
            kec:kecstring,
            kel:desastring,
            alamat:alamat,
            registeredby:{
                role:this.state.pendaftar,
                email:user.email,
                nama:user.name
            },
            islemkari:this.state.islemkari,
            pengprov:this.state.pengprovstring,
            pengcab:this.state.pengcabstring,
            isbeladirilain:this.state.isbeladirilain,
            dojo:this.state.dojo,
            perguruan:this.state.perguruan,
            pengalamanlain:this.state.pengalamanlain,
            photo:this.state.photourl
            };
            console.log(tampungan)
        // this.storetoasync(tampungan);
        this.sendtoasync(JSON.stringify(tampungan));
  }
  async getuser(){
     var hehe = await AsyncStorage.getItem('@userinfo');
     var by = JSON.parse(hehe);
     this.onRegis(by);
  }
  
  async sendtoasync(data){
    await AsyncStorage.setItem('@registereduser',data);
    this.showtampungan();
  }
  async showtampungan(){
    var hehe = await AsyncStorage.getItem('@registereduser');
    if(this.props.pendaftar==='diri sendiri'){
        console.log(JSON.parse(hehe));
    }else{
        Actions.lanjutkan({pendaftar:this.props.pendaftar});
    }
  }
  setvalue(value){
        if(value==='true'){
            this.setState({
                islemkari:true,
                showquestionbeladiri:false,
                islemkarival:'true',
                isbeladirilain:false
            });
        }else{
            this.setState({
                islemkari:false,
                showquestionbeladiri:true,
                islemkarival:'false'
            });
        }
    }
    setbeladirilain(value){
        if(value==='true'){
            this.setState({
                isbeladirilain:true,
                isbeladirilainval:'true'
            });
        }else{
            this.setState({
                isbeladirilain:false,
                isbeladirilainval:'false'
            });
        }
    }

    render() {

        return (
            <Container>
                <Header style={{ backgroundColor: '#5a113d',top:20 }}>
                    <Left>
                        <Button transparent style={{top: 5}}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title> Pendaftaran </Title>
                    </Body>
                </Header>
                <View style={{ justifyContent: 'center', flexDirection: 'row' , top:21}} >
                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/wann.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }} > Info </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/numtu.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Tambah </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/tiga.png')} style={{ width: 25, height: 25 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Isi Form </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/numfo.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Kirim </Text>
                    </Button>
                </View>
                <Content style={{ top: 20 }}>
                    <View style={{ left: 30 }}>
                        <Text> Silahkan isi data data berikut dengan Benar dan Lengkap ! </Text>
                        <Text style={{ color: 'red' }}> * Wajib </Text>
                    </View>


                    <Text style={{left:30, top: 20, color: '#707070' }}> Nama Lengkap ( Sesuai KTP / Kartu Pelajar )<Text style={{color: 'red' }}> *</Text></Text>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                </View>
                    <TextInput underlineColorAndroid='#969696'
                        placeholder="Nama Lengkap"
                        onChangeText={(nama) => this.setState({ nama })}
                        placeholderTextColor="#969696"
                        keyboardType="default"
                        style={{ width: 300, left: 30, margin:5 }}>
                    </TextInput>

                    <Text style={{left:30,top: 20, color: '#707070', marginBottom:20 }}> Jenis Kelamin<Text style={{color: 'red' }}> *</Text></Text>
                    <RadioButton.Group
                        onValueChange={value => this.setState({ value })}
                        value={this.state.value}

                    >
                        <View flexDirection='row' style={{left:30}}>
                            <RadioButton value="Laki-Laki" />
                            <Text style={{ top: 10, }}>Laki-Laki</Text>

                        </View>
                        <View flexDirection='row' style={{ top: -35, left: 220 }}>
                            <RadioButton value="Perempuan" />
                            <Text style={{ top: 10 , }}>Perempuan</Text>

                        </View>
                    </RadioButton.Group>
                    <Text style={{ left:30, color: '#707070', top: -20,marginBottom:10 }}> Tempat Lahir<Text style={{color: 'red' }}> *</Text></Text>
                    <TextInput underlineColorAndroid='#969696'
                        placeholder="Purbalingga"
                        placeholderTextColor="#969696"
                        keyboardType="default"
                        onChangeText={(tempatlahir) => this.setState({ tempatlahir })}
                        style={{ width: 134, left: 30 , top:-40}}>
                    </TextInput>
                    <Text style={{ top: -95, color: '#707070', left: 200}}> Tanggal Lahir<Text style={{color: 'red' }}> *</Text></Text>
                    <View >
                        <DatePicker
                            style={{ top: -93, left: 195 }}
                            date={this.state.date}
                            placeholder="22/03/1999"
                            format="DD/MM/YYYY"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            onDateChange={(date) => { this.setState({ date: date }); }}
                        />

                    </View>
                    <Text style={{left:30, top: -98,color: '#707070' }}> Provinsi<Text style={{color: 'red' }}> *</Text></Text>
                   {this.createpicker()}
                   <Text style={{left:30, top: -98,color: '#707070' }}> Kabupaten<Text style={{color: 'red' }}> *</Text></Text>
                    {
                        this.state.iskab&&
                        <Picker
                        selectedValue={this.state.kabstring}
                        onValueChange={kabstring=>this.getkec(kabstring)}
                        style={{ left:30,alignItems:'center', top: -100}}
                         >
                         {this.state.kab.map((item, key) =>  <Picker.Item label={item.nama} value={item.nama} />)}
                      
                        </Picker>
                    }
                    <Text style={{left:30, top: -98,color: '#707070' }}> Kecamatan<Text style={{color: 'red' }}> *</Text></Text>
                    {
                        this.state.iskec&&
                        <Picker
                        selectedValue={this.state.kecstring}
                        onValueChange={kecstring=>this.getdes(kecstring)}
                        style={{ left:30,alignItems:'center', top: -100}}
                         >
                         {this.state.kec.map((item, key) =>  <Picker.Item label={item.nama} value={item.nama} />)}
                        </Picker>
                    }
                     <Text style={{left:30, top: -98,color: '#707070' }}> Desa / Kelurahan<Text style={{color: 'red' }}> *</Text></Text>
                    {
                        this.state.isdesa&&
                        <Picker
                        selectedValue={this.state.desastring}
                        onValueChange={desastring=>this.setState({desastring})}
                        style={{ left:30,alignItems:'center', top: -100}}
                         >
                         {this.state.desa.map((item, key) =>  <Picker.Item label={item.nama} value={item.nama} />)}
                        </Picker>
                    }
                    <Text style={{left:30,top: -90,color: '#707070',marginBottom:10 }}> Alamat<Text style={{color: 'red' }}> *</Text></Text>
                    <TextInput underlineColorAndroid='#969696'
                        placeholder="Alamat Lengkap"
                        onChangeText={(alamat) => this.setState({ alamat })}
                        placeholderTextColor="#969696"
                        keyboardType="default"
                        style={{ width: 300, left: 30,top: -90,}}>
                    </TextInput>

                    <Text style={{left:30, top: -90,color: '#707070', marginBottom:10 }}>Apakah Anda Anggota Lemkari?<Text style={{color: 'red' }}> *</Text></Text>
                    <View style={{top:-90}}>
                        <RadioButton.Group
                            onValueChange={islemkari => this.setvalue(islemkari)}
                            value={this.state.islemkarival}>

                            <View flexDirection='row' style={{left:30}}>
                                <RadioButton value='true' />
                                <Text>Ya</Text>

                            </View>
                            <View flexDirection='row' style={{ top: -35, left: 220 }}>
                                <RadioButton value='false' />
                                <Text>Bukan</Text>
                            </View>

                    </RadioButton.Group>
                    </View>
                    {
                        this.state.islemkari &&
                            <View>
                                <Text style={{left:30, top: -90,color: '#707070' }}> Pengprov<Text style={{color: 'red' }}> *</Text></Text>
                                <Picker
                                selectedValue={this.state.pengprovstring}
                                onValueChange={pengprovstring=>this.getpengcab(pengprovstring)}
                                style={{ left:30,alignItems:'center', top: -90}}
                                 >
                                 {this.state.provinsi.map((item, key) =>  <Picker.Item label={item.nama} value={item.nama} />)}
                                </Picker>
                                <Text style={{left:30, top: -90,color: '#707070' }}> Pengcab<Text style={{color: 'red' }}> *</Text></Text>
                                {
                                    this.state.ispengcab&&
                                    <Picker
                                    selectedValue={this.state.pengcabstring}
                                    onValueChange={pengcabstring=>this.setState({pengcabstring})}
                                    style={{ left:30,alignItems:'center', top: -90}}
                                     >
                                     {this.state.pengcab.map((item, key) =>  <Picker.Item label={item.nama} value={item.nama} />)}
                                    </Picker>
                                }
                                <Text style={{left:35, top: -90, color: '#707070' }}>Dojo<Text style={{color: 'red' }}> *</Text></Text>
                                <TextInput underlineColorAndroid='#969696'
                                    placeholder="Dojo"
                                    onChangeText={(dojo) => this.setState({ dojo })}
                                    placeholderTextColor="#969696"
                                    keyboardType="default"
                                    style={{ width: 300, top: -90 ,left: 35 }}>
                                </TextInput>
                                <Text style={{left:30, top: -90,color: '#707070' }}> Sabuk<Text style={{color: 'red' }}> *</Text></Text>
                                    <Picker
                                    selectedValue={this.state.sabuk}
                                    onValueChange={sabuk=>this.setState({sabuk})}
                                    style={{ left:30,alignItems:'center', top: -100}}
                                     >
                                     <Picker.Item label='Putih' value='Putih' />
                                     <Picker.Item label='Kuning' value='Kuning' />
                                     <Picker.Item label='Hijau' value='Hijau' />
                                     <Picker.Item label='Biru' value='Biru' />
                                     <Picker.Item label='Coklat' value='Coklat' />
                                     <Picker.Item label='Dan' value='Dan' />
                                    </Picker>
                            </View>
                            }
                            {this.state.showquestionbeladiri && 
                            <>
                            <Text style={{left:30,top: -90, color: '#707070', marginBottom:10 }}>Apakah Anda Pernah Ikut Beladiri lain ?<Text style={{color: 'red' }}> *</Text></Text>
                                <View style={{top:-90}}>
                                <RadioButton.Group
                                    onValueChange={beladirilain => this.setbeladirilain(beladirilain)}
                                    value={this.state.isbeladirilainval}>
                                    <View flexDirection='row' style={{left:30}}>
                                        <RadioButton value='true' />
                                        <Text style={{ top: 10, }}>Ya</Text>

                                    </View>
                                    <View flexDirection='row' style={{ top: -35, left: 220 }}>
                                        <RadioButton value='false' />
                                        <Text style={{ top: 10 , }}>Tidak</Text>
                                    </View>
                                </RadioButton.Group>
                                </View>
                                </>
                            }
                    
                    {
                        this.state.isbeladirilain &&
                        <View>
                                <Text style={{left:30, top: -90, color: '#707070' }}>Perguruan<Text style={{color: 'red' }}> *</Text></Text>
                                <TextInput underlineColorAndroid='#969696'
                                    placeholder="Perguruan"
                                    onChangeText={(perguruan) => this.setState({ perguruan })}
                                    placeholderTextColor="#969696"
                                    keyboardType="default"
                                    style={{ width: 300, left: 30, margin:5, top: -90 }}>
                                </TextInput>
                                <Text style={{left:30, top: -90,color: '#707070' }}> Pengprov<Text style={{color: 'red' }}> *</Text></Text>
                                <Picker
                                selectedValue={this.state.pengprovstring}
                                onValueChange={pengprovstring=>this.getpengcab(pengprovstring)}
                                style={{ left:30,alignItems:'center', top: -100}}
                                 >
                                 {this.state.provinsi.map((item, key) =>  <Picker.Item label={item.nama} value={item.nama} />)}
                                </Picker>
                                <Text style={{left:30, top: -90,color: '#707070' }}> Pengcab<Text style={{color: 'red' }}> *</Text></Text>
                                {
                                    this.state.ispengcab&&
                                    <Picker
                                    selectedValue={this.state.pengcabstring}
                                    onValueChange={pengcabstring=>this.setState({pengcabstring})}
                                    style={{ left:30,alignItems:'center', top: -90}}
                                     >
                                     {this.state.pengcab.map((item, key) =>  <Picker.Item label={item.nama} value={item.nama} />)}
                                    </Picker>
                                }
                                <Text style={{left:30, top: -90,color: '#707070' }}> Sabuk<Text style={{color: 'red' }}> *</Text></Text>
                                    <Picker
                                    selectedValue={this.state.sabuk}
                                    onValueChange={sabuk=>this.setState({sabuk})}
                                    style={{ left:30,alignItems:'center', top: -90}}
                                     >
                                     <Picker.Item label='Putih' value='Putih' />
                                     <Picker.Item label='Kuning' value='Kuning' />
                                     <Picker.Item label='Hijau' value='Hijau' />
                                     <Picker.Item label='Biru' value='Biru' />
                                     <Picker.Item label='Coklat' value='Coklat' />
                                     <Picker.Item label='Dan' value='Dan' />
                                    </Picker>
                            </View>
                            
                    }
                    <Text style={{left:30,top: -90,color: '#707070',marginBottom:10 }}> Pengalaman Lainnya ( Kosongkan Jika tidak ada )</Text>
                    <TextInput underlineColorAndroid='#969696'
                        placeholder="Pengalaman Lainnya"
                        onChangeText={(pengalamanlain) => this.setState({ pengalamanlain })}
                        placeholderTextColor="#969696"
                        keyboardType="default"
                        style={{ width: 300, left: 30,top: -110,}}>
                    </TextInput>
                    <View style={{ flex: 1, top:-90,alignItems: 'center', justifyContent: 'center' }}> 
                            {
                                this.state.photo && (
                                    <Image source={{ uri: this.state.photo.uri }}
                                    style={{ width: 200, height: 300 }}/>
                                )
                            }
                            <Button vertical style={{ backgroundColor: '#4a0931' }} onPress={this.handleChoosePhoto}><Text style={{ fontSize: 14, color: '#ffffff' }}>Unggah Foto</Text></Button>
                        </View>
                    

                </Content>
                <Footer style={{ backgroundColor: '#4a0931' }}>
                    <FooterTab style={{ backgroundColor: '#4a0931' }}>
                        <Button vertical onPress={this.getuser.bind(this)}>
                            <Text style={{ fontSize: 16, color: '#ffffff' }}>SIMPAN</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}
