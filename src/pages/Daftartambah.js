/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, Picker } from 'react-native';
import { Image } from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import Iconsa from 'react-native-vector-icons/AntDesign';
import Iconsi from 'react-native-vector-icons/MaterialIcons';
import Iconsu from 'react-native-vector-icons/MaterialCommunityIcons';
import Iconse from 'react-native-vector-icons/Foundation';
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
import {Actions} from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { Badge, Card, CardItem, Container, Header, Content, Footer, FooterTab, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Icon, Title, View, Radio, } from "native-base";
export default class Daftartambah extends Component{
    constructor(props) {
        super(props);

        this.state = {
          pendaftar:'orang tua',
          forminput:[{nama:"isi di sini"}],
          isself:false,
          showplusminus:true
        };
}
async componentDidMount(){
    const value =  await AsyncStorage.getItem('@userinfo');
    console.log(JSON.parse(value));
    const hasil = await AsyncStorage.getItem('@registereduser');
    if (hasil!=null){
        this.setState({
            forminput:JSON.parse(hasil)
        });
    }
    let user =this.props.pendaftar;
    if (user!=null){
        this.setState({
            pendaftar:user
        });
    this.cekpendaftar(user);
    }
}

  addform = () =>{
    this.setState({
      forminput: this.state.forminput.concat([{ nama: "isi di sini" }])
    });
  };

  remform= idx => () => {
    this.setState({
      forminput: this.state.forminput.filter((s, sidx) => idx !== sidx)
    });
  };
 cekpendaftar(pendaftar){
        this.setState({
            pendaftar:pendaftar
        });
        if(pendaftar=='diri sendiri'){
                this.setState({
                    isself:true,
                    showplusminus:false,
                    forminput:[{nama:"isi di sini"}]
                })
            }else{
                this.setState({
                    showplusminus:true,
                    isself:false
                })
            }
    }
     async changependaftar(pendaftar){
        var checking = await AsyncStorage.removeItem('@registereduser');
        this.setState({
            forminput:[{nama:"isi di sini"}]
        }) 
        this.setState({
            pendaftar:pendaftar
        });
        if(pendaftar=='diri sendiri'){
                this.setState({
                    isself:true,
                    showplusminus:false,
                    forminput:[{nama:"isi di sini"}]
                })
            }else{
                this.setState({
                    showplusminus:true,
                    isself:false
                })
            }
    }
   handlekirim = () =>{
        if(this.state.forminput[0].nama==='isi di sini'){
            console.log('belum isi');
        }else{
           fetch('http://192.168.0.22:3000/users/register', {
                method: 'POST',
                 headers: {
                    "Content-Type": "application/json",
                  },
                body: JSON.stringify(this.state.forminput)
              }).then(res => res.json()).then(
              (result) => {
                  console.log(result)
                  if(result.status==='success'){
                      Actions.kirim();
                  }else{
                      console.log('something wrong')
                  }
              },(error) => {console.log(error);});
        }
    }
    
    render() {

        const { navigate } = this.props.navigation;
      
        return (
            <Container>
                <Header style={{ backgroundColor: '#5a113d' }}>
                    <Left>
                        <Button transparent>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title> Pendaftaran </Title>
                    </Body>
                </Header>
                <View style={{ justifyContent: 'center', flexDirection: 'row' , top:10}} >
                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/wann.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }} > Info </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/numtu.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Tambah </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/tiga.png')} style={{ width: 25, height: 25 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Isi Form </Text>
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Iconsi name="more-horiz" style={{ fontSize: 20, color: '#707070' }} />
                    </Button>

                    <Button vertical transparent style={{ backgroundColor: 'white' }}>
                        <Image source={require('../image/numfo.png')} style={{ width: 15, height: 15 }} />
                        <Text style={{ fontSize: 12, color: '#00a1f1' }}> Kirim </Text>
                    </Button>

                </View>

                <View style={{ top: '3%', left:'4%', margin:3 }} >
                    <Text>Pilih Pendaftar                                        jumlah anggota</Text>
                </View>
                <Content>
                    <View style={{ left: '4%', width: 135 }}>
                       <Picker
                          selectedValue={this.state.pendaftar}
                          onValueChange={(pendaftar)=>this.changependaftar(pendaftar)}
                          style={{marginTop:5, width:135, alignItems:'center'}}
                          >
                          <Picker.Item label="orang tua/wali" value="orang tua" />
                          <Picker.Item label="pelatih" value="pelatih" />
                          <Picker.Item label="diri sendiri" value="diri sendiri" />
                        </Picker>
                    </View>
                    
                    <View style={{ width: 128, justifyContent: 'center', top: -35, left: 255 }} flexDirection='row' >
                    {this.state.showplusminus &&
                        <Button  onPress={this.remform(this.state.forminput.length-1)} transparent style={{ width: 22, height: 22 , }}>
                            <Iconsa name='minuscircleo' style={{ fontSize: 20, color: '#a7a7a7' }} />
                        </Button>
                    }
                        <Text style={{ left: 22 }}>{this.state.forminput.length}</Text>
                    {this.state.showplusminus &&
                        <Button transparent onPress={this.addform} style={{ left: 44, width: 22, height: 22 }} >
                            <Iconsa name='pluscircleo' style={{ fontSize: 20, color: '#008466' }} />
                        </Button>
                    }
                    </View>
                    {this.state.isself ?
                    <View>
                            <Card> 
                                <CardItem style={{ width: 332, height: 80, borderRadius: 60, borderStyle: "solid" }}>
                                    <Left>
                                        <Iconsu name="account-box" style={{ fontSize: 30, color: '#757575' }} />
                                    </Left>
                                    <Text onPress={() => Actions.next({index: {key:0},pendaftar:this.state.pendaftar})} style={{ fontSize: 20 }}>Isi Disini</Text>

                                    <Right>
                                        <Button transparent >
                                            <Iconse name ='clipboard-notes' style={{ left: 80, fontSize: 30, color: '#000000' }}></Iconse>
                                        </Button>

                                    </Right>
                                </CardItem>
                            </Card>
                    </View>
                    
                    :
                    <View>
                    {
                        this.state.forminput.map((item, key)=>(
                        <Card> 
                                <CardItem style={{ width: 332, height: 80, borderRadius: 60, borderStyle: "solid" }}>
                                    <Left>
                                        <Iconsu name="account-box" style={{ fontSize: 30, color: '#757575' }} />
                                    </Left>

                                    <Text onPress={() => Actions.next({index: {key},pendaftar:this.state.pendaftar})} style={{ fontSize: 20 }}>{item.nama}</Text>

                                    <Right>
                                        <Button transparent >
                                            <Iconse name ='clipboard-notes' style={{ left: 80, fontSize: 30, color: '#000000' }}></Iconse>
                                        </Button>

                                    </Right>
                                </CardItem>
                        </Card>
                        ))}
                    </View>
                    }
                    <Button vertical onPress={this.handlekirim} >
                      <Text style={{fontSize: 16, color: '#ffffff'}}>KIRIM</Text>
                    </Button>
                </Content>



            </Container>
        );
    }
}
