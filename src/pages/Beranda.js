/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet } from 'react-native';
import { Image , TouchableOpacity,PermissionsAndroid,Alert,ActivityIndicator, Dimensions, Linking } from 'react-native';
import Icons from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import Geolocation from 'react-native-geolocation-service';
import MapView, { Marker } from 'react-native-maps';
import AsyncStorage from '@react-native-community/async-storage';
import { Container, Header, Content, Footer, FooterTab, List, ListItem, Thumbnail, Text, Left, Body, Right, Button, Icon, Title, View,  } from "native-base";

const {width,height} = Dimensions.get('window');

export default class CardItemBordered extends Component {
  constructor(props) {
    super(props);

  
    this.state = {
      region: {
                latitude: "",
                longitude: "",
                latitudeDelta:0.0922,
                longitudeDelta:0.0421,
                accuracy:""},
                listdojo:[],
                bottom: 1 
              };
  }
  goBack() {
    Actions.pop();
  }
 async componentDidMount(){
    this.permissioncheck();
    this.getlistdojo();
    await AsyncStorage.setItem('@userinfo',JSON.stringify(this.props.userinfo));
    await AsyncStorage.removeItem('@registereduser');
  }
  getlistdojo(){
    fetch("http://192.168.0.22:3000/dojo", {
      method: 'GET',
      headers:{
        "Content-type": "application/json"
      }
    }).then(res => res.json())
      .then(
        (result) => {
          console.log('sampai sini')
          if(result.status==="success"){
            this.setState({
              listdojo:result.data
            })
          }else{
          ToastAndroid.show('Password salah atau user belum terdaftar', ToastAndroid.SHORT);
        }
        },
        (error) => {
        }
      )
  }
  permissioncheck(){
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "Location Accessing Permission",
        message: "App needs access to your location"
      }
    ); 
    this.getcurrentlocation();
  }
  getcurrentlocation(){
        Geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                   region:{
                    latitude:position.coords.latitude,
                    longitude:position.coords.longitude,
                    accuracy:position.coords.accuracy,
                    latitudeDelta:0.1322,
                    longitudeDelta:0.0074}                    
                });
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
  } 
   marker(){
      return {
          latitude:this.state.region.latitude,
          longitude:this.state.region.longitude
      }
  }
  clickedmarker(key){
    console.log(this.state.listdojo[key]);
    Alert.alert(
      'Alert',
      'Choose Action',
      [
        {text: 'Navigate to destination',
         onPress: () => Linking.openURL('https://www.google.com/maps/dir/?api=1&origin='+this.state.region.latitude+','+this.state.region.longitude+'&destination='+this.state.listdojo[key].latlong.latitude+','+this.state.listdojo[key].latlong.longitude)},
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'See Profile', onPress: () => this.props.navigation.navigate('profil',{result:this.state.listdojo[key]})},
      ],
      {cancelable: false},
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    console.log(this.props.userinfo);
    return (
      <Container>
        <Header style={{backgroundColor:'#5a113d'}}>
          <Left>
          <TouchableOpacity onPress={this.goBack}>
              <Icon name='arrow-back' style={{fontSize: 30, color: 'white'}} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title>K-DOJO</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name='search' />
            </Button>
          </Right> 
        </Header>

       
           {
                this.state.region.latitude ? 
                <MapView 
                 toolbarEnabled={false}
                 initialRegion={this.state.region}
                 style={{flex: 1 , width:width,bottom:this.state.bottom}}

                  >
                 <MapView.Marker
                    coordinate={this.marker()}
                    title="You"
                    description="You are here!"
                    pinColor='red'
                 />
                 { this.state.listdojo.map((item, key)=>(
                   <MapView.Marker
                    coordinate={item.latlong}
                    title={item.name}
                    description={item.alamat}
                    pinColor='blue'
                    onPress={() => this.clickedmarker(key)}

                 />
                  )
                )}
                </MapView>
                : <ActivityIndicator size="large" color="#0000ff" /> 
            }
      

        <Content>
       

         { 
          this.state.listdojo.map((item, key)=>(
            
           <List>
            <ListItem thumbnail onPress={() => this.props.navigation.navigate('profil',{result:this.state.listdojo[key]})}>
              <Left>
              <View>
                <Image source={{uri:'https://i.ibb.co/09LPvh7/f2.png'}} style={{ width: 80, height: 65}} />
                </View>
              </Left>
              <Body>
                <Text>{item.name}</Text>
                <Text note numberOfLines={1}style={{fontSize: 15, color: '#000'}}>{item.alamat}</Text>
              </Body>
              <Right>
                 <Button transparent style={{justifyContent:'center', height:40}}>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
             <Icons name='star' style={{fontSize: 7, color: '#f7ca18'}}/>
            </Button>
              </Right>
            </ListItem>
          </List>
          )
        )}
        </Content>
         <Footer style={{backgroundColor:'#4a0931'}}>
          <FooterTab style={{backgroundColor:'#4a0931'}}>
            <Button vertical>
              <Icons name="sort" style={{fontSize: 20, color: '#ffffff'}}/>
              <Text style={{fontSize: 12, color: '#ffffff'}}>Sort</Text>
            </Button>
            <Button vertical active>
              <Icons active name="filter" style={{fontSize: 20, color: '#ffffff'}}/>
              <Text style={{fontSize: 12, color: '#ffffff'}}>Filter</Text>
            </Button>
            <Button vertical>
              <Icons name="map-marker" style={{fontSize: 20, color: '#ffffff'}}/>
              <Text style={{fontSize: 12, color: '#ffffff'}}>Maps</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}