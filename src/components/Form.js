import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  Button,
  ToastAndroid
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';


export default class Form extends Component<{}> {
constructor(props) {
    super(props);
  
    this.state = {
      username: '',
      password: '',
      userInfo:'',
    };
  }


  UNSAFE_componentWillMount(){
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId:
        '287386678514-sheide4ibmm693285ucp7nmhrf75r4j3.apps.googleusercontent.com',
    });
    this.getCurrentUserInfo();
  }
  getCurrentUserInfo = async () => {
  try {
    const userInfo = await GoogleSignin.signInSilently();
    this.setState({  userInfo: userInfo  });
    console.log('sudah login');
    console.log(userInfo.user);
    Actions.home({userinfo:userInfo.user});
  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_REQUIRED) {
      console.log('belum login');
    } else {
    }
  }
};
 _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({

        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
      console.log(userInfo);
      Actions.home();
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };

	render(){
		return(
			<View style={styles.container}>
        <GoogleSigninButton
          style={{ width: 192, height: 60 }}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
          onPress={this._signIn}/>
  		</View>
			)
	}
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center',
    
  },

  bgimage :{
    flex :1 ,
    alignSelf:'stretch',
    width: null,
    justifyContent:'center'

  },

  content:{
    alignItems:'center'

  },

  inputBox: {
    width:334,
    height:40,
    backgroundColor:'#ffffff',
    borderRadius: 6,
    borderWidth:1,
    paddingHorizontal:16,
    fontSize:14,
    color:'#373837',
    marginVertical: 10
  },
  button: {
    width:334,
    height:40,
    backgroundColor:'#310522',
     borderRadius: 6,
     borderWidth:1,
      marginVertical: 10,
      paddingVertical: 10
  },
  buttonText: {
    fontSize:16,
    fontFamily: 'sans-serif',
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }

});
