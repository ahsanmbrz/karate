/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ImageBackground
} from 'react-native';


import Routes from './src/Routes';

export default class App extends Component<{}> {
  render() {
    return (
    
      <View style={styles.container}>   
       <StatusBar
           //backgroundColor="#ffffff"
           barStyle="light-content"/>
        <Routes/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex: 1,
  }
});